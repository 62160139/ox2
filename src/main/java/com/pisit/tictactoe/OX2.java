/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pisit.tictactoe;

import java.util.Scanner;

/**
 *
 * @author User
 */
public class OX2 {
    static int count = 0;
    static char winner = '-';
    static boolean isFinish = false;
    static Scanner kb = new Scanner(System.in);
    static int row, col;
    static char[][] table = {
            {'-', '-', '-'},
            {'-', '-', '-'},
            {'-', '-', '-'},
        } ;
    static char player = 'X';
    static void showWelcome(){
        System.out.println("Welcome to OX Game");
    }
    static void showTable(){
        System.out.println(" 123");
        for(int row=0; row<table.length; row++){
            System.out.print(row+1);
            for(int col=0; col<table[row].length; col++){
                System.out.print(table[row][col]);
            }
            System.out.println("");
        }
    }
    static void showTurn(){
        System.out.println(player + " turn");
    }
    static void input(){
        
            while(true){
            System.out.println("Plese input Row Col : ");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;
            count++;
            if(table[row][col] == '-'){
                table[row][col] = player;
                break;
            }
            System.out.println("Error: table at row and col is not empty!!!");
        }
           
    }
    static void checkCol(){
        for(int row=0; row<3; row++){
            if(table[row][col] != player){
                return;
            }
        }
        isFinish = true;
        winner = player;
    }
     static void checkRow(){
        for(int col=0; col<3; col++){
            if(table[row][col] != player){
                return;
            }
        }
        isFinish = true;
        winner = player;
    }
    static void checkWin(){
        checkRow();
        checkCol();
        checkX();
        checkDraw();
    } 
    static void checkX(){
        if(table[0][0] == 'X' && table[1][1] == 'X' && table[2][2] == 'X'
           || table[0][2] == 'X' && table[1][1] == 'X' && table[2][0] == 'X'){
            isFinish = true;
            winner = 'X';
        }else if(table[0][0] == 'O' && table[1][1] == 'O' && table[2][2] == 'O'
           || table[0][2] == 'O' && table[1][1] == 'O' && table[2][0] == 'O'){
            isFinish = true;
            winner = 'O';
        }
    }
    static void switchPlayer(){
        if(player=='X'){
            player='O';
        }else{
            player='X';
        }
    }
    static void checkDraw(){
        if(count==9){
            isFinish = true;
        }
    }
    static void showResult(){
        if(winner=='-'){
            System.out.println("Draw");
        }else{
            System.out.println(winner + " Win!!!");
        }
    }
    static void showBye(){
        System.out.println("Bye bye ...");
    }

    public static void main(String[] args) {
        int count = 1;
        showWelcome();
        try{
          do{
            showTable();
            showTurn();
            input();
            checkWin();
            switchPlayer();
        }while(!isFinish);  
        showResult();
        showBye();  
        }catch(ArrayIndexOutOfBoundsException e){
            System.out.println("Index out of bound");
        }
        
        
    }
    
}
